function welcomeUser() {
    console.log("welcome to me");
}

function setDatePicker() {
    $(document).ready(() => {
        $('#weeklyDatePicker').datepicker();

        $('#weeklyDatePicker').on('change', function(e) {
            var value = $("#weeklyDatePicker").val();
            var firstDate = moment(value, "MM-DD-YYYY").day(0).format("DD-MM-YYYY");
            var lastDate = moment(value, "MM-DD-YYYY").day(6).format("DD-MM-YYYY");
            $("#weeklyDatePicker").prop('value', firstDate + " - " + lastDate);
        });
    })
};