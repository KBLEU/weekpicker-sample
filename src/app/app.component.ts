import { Component } from '@angular/core';
import { ConfigService } from './service/config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-weekpicker';
  api_url: string;

  constructor(private config: ConfigService) {
    this.api_url = this.config.getConfig().api_url;
  }
}
