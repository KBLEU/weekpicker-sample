import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeekPickerComponent } from './cust-components/week-picker/week-picker.component';
import { WeekPicker2Component } from './cust-components/week-picker2/week-picker2.component';
import { DatePickerDirectiveDirective } from './cust-components/date-picker-directive/date-picker-directive.directive';
import { ConfigService } from './service/config.service';
import { HttpClientModule } from '@angular/common/http';

export function initConfig (appConfig: ConfigService){
  return () => appConfig.loadConfig();
}
@NgModule({
  declarations: [
    AppComponent,
    WeekPickerComponent,
    WeekPicker2Component,
    DatePickerDirectiveDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initConfig,
      deps: [ConfigService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
