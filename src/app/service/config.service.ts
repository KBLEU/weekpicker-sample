
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"

interface AppConfig {
  api_url: string;
}

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private config: AppConfig | any = null;
  loaded = false;


  constructor(private http: HttpClient) { }

  loadConfig(): Promise<void> {
    return this.http
    .get<AppConfig>('/assets/app.config.json')
    .toPromise()
    .then(data => {
        this.config = data;
        this.loaded = true;
    });
  }

  getConfig(): AppConfig {
    return this.config;
  }
}
