import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeekPicker2Component } from './week-picker2.component';

describe('WeekPicker2Component', () => {
  let component: WeekPicker2Component;
  let fixture: ComponentFixture<WeekPicker2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeekPicker2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WeekPicker2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
