import { Component } from '@angular/core';

@Component({
  selector: 'week-picker2',
  templateUrl: './week-picker2.component.html',
  styleUrls: ['./week-picker2.component.css']
})
export class WeekPicker2Component {

  retrieveSelectedWeek(date: string) {
    console.log("selected date ", date);
  }

}
