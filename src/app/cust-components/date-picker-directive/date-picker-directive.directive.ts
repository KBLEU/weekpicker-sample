import { AfterViewInit, Directive, ElementRef, EventEmitter, Output } from '@angular/core';

// binding with jQuery and moment standard variable
declare var $:any;
declare var moment:any;

@Directive({
  selector: '[appDatePicker]',
  exportAs: "week_picker"
})
export class DatePickerDirectiveDirective implements AfterViewInit {

  selectedWeek: any;
  @Output() onSelectWeek = new EventEmitter();

  constructor(private el: ElementRef ) {

  }

   ngAfterViewInit(): void {
    // Create my datepicker
    $(this.el.nativeElement).datepicker();
    // Manage when new element is selected
    $(this.el.nativeElement).on("change", () => {
      var value = $(this.el.nativeElement).val();
      var firstDate = moment(value, "MM-DD-YYYY").day(0).format("DD-MM-YYYY");
      var lastDate = moment(value, "MM-DD-YYYY").day(6).format("DD-MM-YYYY");
      $(this.el.nativeElement).prop('value', firstDate + " - " + lastDate);
      this.selectedWeek = firstDate + " - " + lastDate;
      this.onSelectWeek.emit(this.selectedWeek);
    })
   }

}
