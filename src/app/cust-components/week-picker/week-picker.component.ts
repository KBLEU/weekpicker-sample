import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import 'jquery-ui';
import * as moment from "moment";

// import datepickerFactory from 'jquery-datepicker';
// import datepickerJAFactory from 'jquery-datepicker/i18n/jquery.ui.datepicker-en-GB';
declare function welcomeUser(): void;
declare function setDatePicker(): void;

@Component({
  selector: 'week-picker',
  templateUrl: './week-picker.component.html',
  styleUrls: ['./week-picker.component.css'],
})
export class WeekPickerComponent implements OnInit {

  constructor() {
    welcomeUser();
    setDatePicker();

  }
  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.

  }
}
